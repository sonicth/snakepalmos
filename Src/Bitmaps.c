#include <PalmOS.h>
#include "snakeCwarrior.h"

#define BPP 8
#define BMPSIZ (CELL_DIM_X * CELL_DIM_Y)
#define PIXEL_THRES 0x02

extern char debugStr[];
extern BitmapType *cellBmpPtrArray[];
extern levelsData[][CELL_COUNT_Y][CELL_COUNT_X];

void LoadSnakeBitmaps(BitmapType * snakeRscBmp)
{
	int i, size, bmpOffs = 0;
	UInt16 error;
	void *rscBmp = BmpGetBits(snakeRscBmp);	
		/*	Check if it has 8bpp
		*/
	#define SNK_WIDTH	8
	#define SNK_HEIGHT	40
	size = BmpBitsSize (snakeRscBmp);
	if (size != SNK_WIDTH*SNK_HEIGHT)
	{
		StrPrintF(debugStr, "can only deal with %d bit bitmaps!", 8, size*8/(SNK_WIDTH*SNK_HEIGHT));
		DispText2(debugStr);
		return;
	}
	
	for(i = SNK_BODY_V; i <= SNK_BENT_SW; i++)
	{
		cellBmpPtrArray[i] = BmpCreate(CELL_DIM_X, CELL_DIM_Y, BPP, NULL, &error);
	}
	
	TransCopyBmp(BmpGetBits(cellBmpPtrArray[SNK_BODY_H]), rscBmp, bmpOffs, IDENT, BmpGetBits(cellBmpPtrArray[CELL_BG]));
	TransCopyBmp(BmpGetBits(cellBmpPtrArray[SNK_BODY_V]), rscBmp, bmpOffs, ROT_90, BmpGetBits(cellBmpPtrArray[CELL_BG]));
	bmpOffs += BMPSIZ;
		/*	due to matching offsets should work right */
	for(i = SNK_HEAD_N; i <= SNK_HEAD_W; i++)
	{
		TransCopyBmp(BmpGetBits(cellBmpPtrArray[i]), rscBmp, bmpOffs, IDENT+(i-SNK_HEAD_N), BmpGetBits(cellBmpPtrArray[CELL_BG]));
		
	}
	bmpOffs += BMPSIZ;
	for(i = SNK_TAIL_N; i <= SNK_TAIL_W; i++)
	{
		TransCopyBmp(BmpGetBits(cellBmpPtrArray[i]), rscBmp, bmpOffs, IDENT+(i-SNK_TAIL_N), BmpGetBits(cellBmpPtrArray[CELL_BG]));
	}
	bmpOffs += BMPSIZ;
	for(i = SNK_BENT_NW; i <= SNK_BENT_SW; i++)
	{
		TransCopyBmp(BmpGetBits(cellBmpPtrArray[i]), rscBmp, bmpOffs, ROT_360+(i-SNK_BENT_NW), BmpGetBits(cellBmpPtrArray[CELL_BG]));		
	}
	bmpOffs += BMPSIZ;
}

void LoadStaticBitmaps(BitmapType * staticRscBmp)
{
	int i, size;
	UInt16 error;
	void *rscBmp = BmpGetBits(staticRscBmp);
		
		/*	Check if it has 8bpp
		*/
	#define SNK_WIDTH	8
	#define SNK_HEIGHT	40
	size = BmpBitsSize (staticRscBmp);
	if (size != SNK_WIDTH*SNK_HEIGHT)
	{
		StrPrintF(debugStr, "can only deal with %d bit bitmaps!", 8, size*8/(SNK_WIDTH*SNK_HEIGHT));
		DispText2(debugStr);
		return;
	}
		/*	no transforms, can do in one move! */
	for(i = CELL_BG; i <= CELL_WALL; i++)
	{
		cellBmpPtrArray[i] = BmpCreate(CELL_DIM_X, CELL_DIM_Y, BPP, NULL, &error);
		TransCopyBmp(BmpGetBits(cellBmpPtrArray[i]), rscBmp, (i-CELL_BG)*BMPSIZ, IDENT, BmpGetBits(cellBmpPtrArray[CELL_BG]));
	}
}

void TransCopyBmp(void *rawDest, void *rawRscSrc, int bmpOffset, transType myTrans, void *backGround)
{
	int i, j, width = CELL_DIM_X, height = CELL_DIM_Y;
	unsigned char *byteD = (unsigned char *) rawDest, *byteS = (unsigned char *) rawRscSrc; //no need using void ptr *bgBmp = (char *) backGround;
	
		//###if it Is background, just identity, empty operation, but no penalty to performance, so let it be!
	MemMove((void *)byteD, backGround, CELL_DIM_X * CELL_DIM_Y);	
		//offset to the right position in resource bitmap
	byteS += bmpOffset;
									//radio - londonhempfair.com in october 2005
	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++)
		{
			switch(myTrans)
			{
				case NOP:
					break;
				case ROT_90:
						/*
						v1[j][i] => v2[w-1-i][j];
						v1[j][i] == v1[width*i+j];
						*/
					if (byteS[width*i+j] > PIXEL_THRES)
						byteD[width*j+(width-1-i)] = byteS[width*i+j];
					break;
				case ROT_270:
						/*
							it is interesting here as the width has to
							stick with j, and height with i!
							all the same for cube!
						v1[j][i] => v2[i][w-1-j]
						*/
					if (byteS[width*i+j] > PIXEL_THRES)
						byteD[width*(width-1-j)+i] = byteS[width*i+j];
					break;
				case MIRROR_HORZ:
					if (byteS[width*i+j] > PIXEL_THRES)
						byteD[width*i+(width-1-j)] = byteS[width*i+j];
					break;
				case ROT_180:
				case MIRROR_HZVT:
					if (byteS[width*i+j] > PIXEL_THRES)
						byteD[width*(height-1-i)+(width-1-j)] = byteS[width*i+j];
					break;
				case MIRROR_VERT:
					if (byteS[width*i+j] > PIXEL_THRES)
						byteD[width*(height-1-i)+j] = byteS[width*i+j];

					break;
				case IDENT:
				case ROT_360:
				default:
					if (byteS[width*i+j] > PIXEL_THRES)
						byteD[width*i+j] = byteS[height*i+j];
			}
		}
	}
}  

void LoadLevels(BitmapType *levelBitmaps)
{
		/*
		assuming bpp is 8, witdth is CELL_COUNT_X, height: CELL_COUNT_Y*MAX_LEVELS
		*/
	void *bmpsDataPtr = BmpGetBits(levelBitmaps);
	
	MemMove((void *)levelsData, bmpsDataPtr, CELL_COUNT_X * CELL_COUNT_Y * MAX_LEVELS);	
}