// snakeCwarrior.h
//
// header file for snakeCwarrior
//
// This wizard-generated code is based on code adapted from the
// stationery files distributed as part of the Palm OS SDK 4.0.
//
// Copyright (c) 1999-2000 Palm, Inc. or its subsidiaries.
// All rights reserved.

#ifndef __SNAKE_GAME__
#define __SNAKE_GAME__


#ifndef FOUR_BENDS
#define FOUR_BENDS
#endif

// ********************************************************************
// Internal Structures
// ********************************************************************

typedef struct snakeCwarriorPreferenceType
{
	UInt8 replaceme;
} snakeCwarriorPreferenceType;

// ********************************************************************
// Global variables
// ********************************************************************

extern snakeCwarriorPreferenceType g_prefs;

// ********************************************************************
// Internal Constants
// ********************************************************************

#define appFileCreator			'STR3'
#define appName					"snakeCwarrior"
#define appVersionNum			0x01
#define appPrefID				0x00
#define appPrefVersionNum		0x01


	////temporary, dont know how to get palm window width
	//window width, height
#define	WIN_WIDTH		160
#define	WIN_HEIGHT		160
	//window offset, e.g. it starts ad (x,y)
#define WIN_X_OFFSET	0
#define WIN_Y_OFFSET	0
	//cell partitioning: number along the width and width of a cell
#define CELL_COUNT		20
	//for compatiblity with future versions, when cell_count may be different!
#define CELL_COUNT_X	CELL_COUNT
#define CELL_COUNT_Y	CELL_COUNT
#define CELL_DIM		(WIN_WIDTH / CELL_COUNT)	
#define CELL_DIM_X		CELL_DIM
#define CELL_DIM_Y		CELL_DIM
	//starting position
#define DEF_START_X_POS		(WIN_WIDTH	/ (2 *	CELL_DIM_X))
#define	DEF_START_Y_POS		(WIN_HEIGHT / (2 *	CELL_DIM_Y))
	//hard key mappings
#define DATE_BOOK_KEY		LEFT
#define ADDRESS_BOOK_KEY	RIGHT
#define TODO_KEY			UP
#define NOTE_KEY			DOWN
	//number of cells
#define DEFAULT_SPEED		12
	//upper limit for cells: for level...
#define MAX_LEVEL_CELLS		4
	//...and globaly
#define MAX_CELLS			20
	//number of levels
#define MAX_LEVELS			10
#define START_LEVEL			1

/*
		macros
*/

#define SWAP(X, Y, TMP)		TMP = X; X = Y; Y = TMP
#define SQR(X)				((X) * (X))
#define ABS(X)				(((X) > 0) ? X : -(X))
	/*	transformation for next position in bent
		{0, 1, 4, 8} -> {-4, -8, 0, -1}
	*/
#define _TURN2(X)		(((X) < 4) ? -((X + 1) << 2) : -((X) >> 3))
	/*	calculate wall
	*/
#define _IS_WALL(BYTE)	(BYTE != 0x00)
	/*	a better 'function' is to have return a free cell
		NOTE: level starts from one!, so decresing for index;
	*/
#define _IS_FREE(X, Y, LEVEL)	(levelsData[LEVEL-1][Y][X] == 0x00)
	/*	inverse position
	*/
#define _INV_VECTOR(POS)	( ABS(_TURN2(POS)) )

	//bitmaps

typedef enum
{
	GAME_SHUTDOWN,
	GAME_other,
	GAME_RUN,
	GAME_MANUAL_EXIT = 5
} runStatusType;

typedef enum
{
	SAFE = 0,
	GROW,
	CRUSHED = 8,	//all values greater than 8 are a crush
	WALL_CRASHED,
	OUTSIDE		
} crashType;

	/*     |1
		4-- -- 0
		   |8
	*/
typedef enum
{
	RIGHT	= 0,
	UP,
	LEFT	= 4,
	DOWN	= 8
} moveVectType;
	/*
		four directions, summs in vertical/horizontal, e.g. opposite directions
		should cancel out as snake cannot move backwards
		ref. CalcMovePosition()
	*/
#define HORIZONTAL_MOVE_SUM	(UP + DOWN)
#define VERTICAL_MOVE_SUM	(RIGHT + LEFT)

//position of the snake cell
typedef enum
{
	VERT = 0,
	HORZ,
	NONE
} cellPosType;
//position of bent
typedef enum
{
	NORTHWEST = 0,	//10:30, 135* (degrees)
	NORTHEAST,		//13:30, 45*
	SOUTHEAST,		//17:30, 315*
	SOUTHWEST		//19:30, 225*
} cellPosBentType;


typedef enum
{
	SNK_BODY_V,
		SNK_BODY_H,			
	SNK_HEAD_N,
		SNK_HEAD_E,
		SNK_HEAD_S,
		SNK_HEAD_W,
	SNK_TAIL_N,
		SNK_TAIL_E,
		SNK_TAIL_S,
		SNK_TAIL_W,
	SNK_BENT_NW,		//NOTE! -Pi/4 radiants for bents
		SNK_BENT_NE,
		SNK_BENT_SE,
		SNK_BENT_SW,
	CELL_BG,			//these dont have 
	CELL_FOOD,
	CELL_WALL,
	CELL_BITMAP_MAX,
	SNK_DFT = SNK_HEAD_N
} cellBitmapType;

typedef enum
{
	IDENT,
	ROT_90,
	ROT_180,
	ROT_270,
	ROT_360,	//identity undercover
	MIRROR_HORZ,
	MIRROR_HZVT,
	MIRROR_VERT,
	NOP
} transType;
	

typedef enum
{
	NOCOLOUR,
	COLOUR
} colSupport;

typedef UInt16 cellStatusType;

typedef struct
{

	Int16 x, y;
	cellStatusType cellStatus;
} cellDataType;

//! do not use typedef in other projects, no need, and less readable!
struct movePosition
{
	moveVectType posNext, posPrev, posHead;
};


//too complicated :: typedef MemHandle cellEntryData;


typedef struct
{
		//
	cellDataType cellData;
		//next element
	Int32 facePos;
	MemHandle Next;

} cellEntryType;

struct gameStatusSt
{

	runStatusType gameStatus;
	UInt16 levelCount;
	UInt16 cellCount;
	UInt16 levelMaxCells;
	UInt16 gameSpeed;
	
	Int32 currScores;
	union
	{
			UInt32 allFlags;
			struct
			{
				char DeviceHardware, ScreenSize, Power, Colour;
			} deviceFlags;	//colour support goes here too
	} unFlags;
};


//------------------------------------------------------------//

	/*
		functions
	*/
	//hard key events
static Boolean HardKeyHandleEvent(EventPtr);
	//list manipulating
UInt16 AddNewCell(Int16, Int16, moveVectType);
void FlushList(void);
void RunLoop(void);
void Initialise(void);
void StopGame();
void MoveCell(void);
crashType CellCrash(Int16, Int16);
void FlushList(void);
Int16 InWindowLimits(Int16, Int16);
void DrawRootCell(void);
void EraseLastCell(void);
void GenNewGrowCell(void);
UInt16 FindCellInList(Int16, Int16);


void SetColour(void);
void DispText(const char *);
void DispText2(const char *);
void DrawBackGround(void);

	//bitmaps
Err		LoadBitmaps(void);
void LoadSnakeBitmaps(BitmapType *);
void LoadStaticBitmaps(BitmapType *);
void TransCopyBmp(void *, void *, int, transType, void *);
void	UnloadBitmaps(void);
BitmapType * GetHeadBitmapPtr(struct movePosition *);
BitmapType * GetBodyBitmapPtr(struct movePosition *);
BitmapType * GetTailBitmapPtr(cellEntryType *);
BitmapType * BentBitmapAlgorithm(moveVectType, moveVectType);
void DrawCell(cellEntryType *, BitmapType *); //simply draw bitmap 

void LoadLevels(BitmapType *);

void LauncherExit(void);
void StoreScores(void);
void CalcScores(crashType);

	//helper functions
const char * vStr(moveVectType);
Boolean FalseHardkeyEvent(EventPtr);


	//database functions
void DbPrintScores(PointType *);
int DbStoreCurrScore(Int32 *);
Err OpenDatabase(void);
void CloseDatabase(void);
void DrawAboutFormStuff(void);

//particular files to snake - warrior

/*
		code warrior definitions*/
   

//*/
runStatusType StopGameDialog(UInt16);

#endif

