#include <PalmOS.h>
#include "snakeCwarrior.h"

	/*
		14 june 2005
		Do not get confused, this functions detects the hardkey falseEvents
		"false" bit does not play significant role in here.
		
		A proper use would be to simply ignore hardkey falseEvents.
	*/
Boolean FalseHardkeyEvent(EventPtr falseEvent)
{
			/*
				some sort of key falseEvent */
	if ( falseEvent->eType == keyDownEvent \
			/*
				macro that tests using chr and modifiers
				that pressed key is a hard key */
&& TxtCharIsHardKey(falseEvent->data.keyDown.modifiers, falseEvent->data.keyDown.chr))
 
		return true;
	else
		return false;
}
			/*
				an example of use in */
//if (FalseHardkeyEvent(&falseEvent) || !SysHandleEvent(&falseEvent))