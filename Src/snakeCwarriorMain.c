/*
	Snake Game, January 2005, (C) Michael Vasiljevs

	Main C source file. Requireds, Header with data structures, resource information.
	Compiles under GNU 68k compiler and under CodeWarrior Metrowerks (Demo version)
	compiler.
	
	Code is not Lisenced under any terms so far.
	However you may assume that it is lisenced under GNU Public Lisence
	if you have obtained the copy of the code.
	
	If you use any portion of the code for your purporses, please give credit.
	
	*No responsibilities from the use of the code whatsoever!
	
	All the game code, including game event loop done by myself, except for some form handling
	which I found in examples and modified.
*/


#include <PalmOS.h>
#include "snakeCwarrior.h"
#include "snakeCwarriorRsc.h"


//#include "ScoreDbase.h"

#define assert(expr,msg) ErrFatalDisplayIf(!(expr),msg)


/*	CHANGE LOG:
	
	25/01/2005, to be changed:
	+remove the memory leaks, when the HardKey is disabled, applications quites,
		resulting in memory leaks
	+other memory errors, learn how to use debugger/IDE
	
	+*enable custom bitmaps on bending
	+enable proper user feedback on levels, etc.
	+enable storage of gaming results in the database.
	*major improvement: think of better algoritm!
	
	22/03/2005,
	+memory leaks removed: chaned the event loop, release resource mandle on exit
	+colour still to be enabled:
		~tricky, for colour mode, if I have 16bit, only get 8bit colour image
			if I have 8 bit only get 4-bit colour image and so on!
		~2-bit colour bitmaps are not displed on PalmVx
	+user feedback: message on termination of the game
	+more user feedback, on the begginning and end
	+a way to quick application without pressing button?

	
	+automatic quit - to launcher
	
	06/04/2005
	~Array of Pointers/Handles of bitmaps
	
	08/04/2005
	+array of pointers and bitmaps
	
	3 main things are left to be done:
	*Scores
	*Bitmaps, including all representation of snake
	*Colour
		
	-problem arises when user presses hard button while next level dialog is on
	+above, sort of fixed
	
	+!divide by zero still haunts Snake, happens WHEN the X=0!!!
	
	26/May/2005
	
	Restructuring Event Handling:
	+Each form will not have separate event handler
	+FrmCloseAllForms() on exit
	

	15/June/2005
	
	The basic stucture is done,
	-fix hardkey on menu button
	+colour + colour bitmaps
	
	12/Oct/2005
	
	+research on how to flip Horz/Vert bitmaps to reduce the necessity to
		copy NW, SE etc 4 times at resource level!
	+research is done, now it's implementation time!
	The only way to do flip is by manual mirroring of bitmaps
	To reduce complexity and overhead, removing unnecessary bitmaps.
	Only two bitmaps that hold Transfromed and Static bitmaps
		1) Snake (tranfromed):
		~Body (V/H)
		~Head(4xCompass)
		~Tail(4xC)
		~Bent(4xC-Pi/4)
		2) Static:
		~Background (noT)
		~Food/Grow token (noT)
		~Other, like wall (noT)
		
	19/Oct/2005
	+Setup Load*Bitmap() routines that create new bitmaps manually
		Tranfromation routines for Mirroring bitmaps
	20/Oct/2005
	All tranfromation routines for Rotation in all directions 180 degrees == double mirror (v+h)
	LoadHead routine, returns pointer to rightly rotated bitmap for head.
	*Bug: cannot store the Enum variable in the link of the list and than read it reliably!
	
	+* Added the rotation of snake: e.g. going one side leaving the other! BUT!!!
		=> cannot exit the game!!!
		
	23/Oct/2005
	
	Found out how to use lanucher in the game, as described near the code, it works like this:
		1)penUpEvent handled by SysHandleEvent(), which detects a soft button press on grafiti,
		2)keyDownEvent() with chr set to 264 in structure [see event header file!]
		Too bad I had to hack it trough, could have not wasted time!!!
	+	Still got to setup proper exit sequence.
		there is a bit of mess with enum runStatusType and gameStatus* menu defines in
		resource hearder file.
		*For now using a simple exit in main loop
		
	01/Nov/05
	
	+try add levels + walls
	 Levels are done!!!
	 
	 
	20/Nov/05
	
	+fixed the tail
	+start position is chosen at random is default is taken by Level Wall
	
	
	13/dec/05
	
	level dificulty:
	+added variable speed 
	+added variable snake length
	*/


void DispText(const char *);
//////////////////////////////////////////////////////////////////
//global vars
//////////////////////////////////////////////////////////////////
char debugStr[128];
	// First AND Last elements in the list 
MemHandle	RootHandle =	NULL, \
			LastHandle =	NULL; 
	//bitmap (arrays)
BitmapType *cellBmpPtrArray[CELL_BITMAP_MAX + 1];
	//info for levels, walls etc.
char levelsData[MAX_LEVELS][CELL_COUNT_Y][CELL_COUNT_X];

	//game speed

UInt32 setDepth = 0x8000;

Boolean initialised = false;
Boolean bitmapsLoaded = false;

//moveVectType moveVector =	STRAIGHT;

	//head position
cellPosType headPosition = NONE;

Int16	curPosX, curPosY;

Int16	growTokenX, growTokenY;

struct movePosition snakeMovPos;

struct gameStatusSt gameStatusData = 
{	
	GAME_RUN,		//Game Status: 23/Oct/2005: changed, see runStatusType
	0,		//0 level
	0,		//length, before initialised, 0 cells
	MAX_LEVEL_CELLS,		//cell limit for level
	DEFAULT_SPEED,	//NOTE: larger==slower
	0,		//0 scores
	0x0		//zero flags!
};		

	
//nasty bug (glitch) that remembers last action for last level; here is quick fix
static WChar keychr = (WChar) NULL;

//////////////////////////////////////////////////////////////////
//standart palm functions
//////////////////////////////////////////////////////////////////
static Boolean MainFormEventHandler(EventType * eventP)
{
    Boolean handled = false;
    FormType * frmP;

    switch (eventP->eType) 
        {
        case menuEvent:
   			break;

     
		case frmOpenEvent:
			frmP = FrmGetActiveForm();
			FrmDrawForm(frmP);
				//initialise the game
			Initialise();
			handled = true;
			break;
		case frmCloseEvent:
			/*
				this event actually closes the form
			*/
			frmP = FrmGetActiveForm();
			FrmEraseForm(frmP);
			//FrmDeleteForm(frmP);
			break;
            
        case frmUpdateEvent:
            	/*	To do any custom drawing here, first call 
            		FrmDrawForm(), then do your drawing, and 
            		then set handled to true.
            	*/
			//frmP = FrmGetActiveForm();
			// FrmDrawForm(frmP);
			//DrawDBRecords();
            handled = true;
            break;
            //tap event
		case ctlSelectEvent:
			/*		some practical code
			
			if (eventP->data.ctlSelect.controlID == SecondaryGoToPrimaryButton)
			{
				FrmGotoForm(MainForm);
				handled = true;
			}
			//*/
			break;      
		case penDownEvent:
			switch((eventP->screenX + eventP->screenX * 2) / 80)
			{
				case 0:
					snakeMovPos.posNext = RIGHT;
					break;
				case 1:
					snakeMovPos.posNext = LEFT;
					break;
				case 2:
					snakeMovPos.posNext = UP;
					break;
				case 3:
					snakeMovPos.posNext = DOWN;
					break;
				default:
					break;
			}
			break;
		case penMoveEvent:
			break;
		case penUpEvent:
			break;

        default:
            break;
        }
    
    return handled;
}

static Boolean ScoresFormEventHandler(EventType * EventP)
{
	Boolean handled = false;
	FormType *frmP;
	struct PointType scoreDrwOffset = {5, 15};
	
	switch (EventP->eType)
	{
		case frmOpenEvent:
			frmP = FrmGetActiveForm();
			FrmDrawForm(frmP);
				//draw stuff	//DrawScores();
			DrawAboutFormStuff();
			handled = true;
			break;
		/*	This event does is not sent for a dialog-style form
		case frmCloseEvent:
			frmP = FrmGetActiveForm();
			FrmEraseForm(frmP);
			FrmDeleteForm(frmP);
			break;
		*/
		case ctlSelectEvent:
			if(EventP->data.ctlSelect.controlID == ScoresNewOKButton)
			{
				/*	instead of FrmGotoForm(FormID) use
				*/
				FrmReturnToForm(0);
				
				FrmEraseForm(FrmGetActiveForm());
				Initialise();
				gameStatusData.levelCount = 0;
				handled = true;
			} else	if (EventP->data.ctlSelect.controlID == ScoresExitSnakeButton) {
				LauncherExit();
				handled = true;
			}
			
			break;
		default:
			break;
	}
	return handled;
}
		
	


static Boolean AppHandleEvent(EventPtr eventP)
 {
   UInt16 formId;
   Boolean handled = false;
   FormType *frmP;

	switch(eventP->eType)
	{
		case frmLoadEvent:
				// Initialize and activate the form resource. 
			formId = eventP->data.frmLoad.formID;
			frmP = FrmInitForm(formId);
			FrmSetActiveForm(frmP);

			switch(formId)
			{
				case MainForm:
					FrmSetEventHandler(frmP, MainFormEventHandler);
					break;
				case ScoresForm:
					FrmSetEventHandler(frmP, ScoresFormEventHandler);
					break;
				default:
					break;
			}
				//initialise data and screen
			handled = true;
			break;
			
		case appStopEvent:	
				/*	20 May 2005:	
					*cleaner interface for switching forms
					*terimantion of game needs to be done separately
				*/
			StopGame();
				
				/*	26/May/2005
					
					FrmCloseAllForms() will attempt to close
					MainForm, thus attempting to release the memory
					that has been freed by FrmDeleteForm();
					
					may use FrmCloseAllForms(), but then do not delete main form!
					instead of
							frmP = FrmGetActiveForm();
							FrmEraseForm(frmP);
							FrmDeleteForm(frmP);
				*/
			FrmCloseAllForms();
			
			handled = true;
		default:
			break;
	}
    
  return(handled);
}

UInt32 PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
 {
	EventType event;
	Err err = 0;
	Boolean handled = false;
	UInt16 tenthOfaSecTick = SysTicksPerSecond() / 20;

   switch (cmd)
     {
       case sysAppLaunchCmdNormalLaunch:			
			FrmGotoForm(MainForm);
      
			do
			{
				static UInt16 MenuError;
					/*
						Trickiest part of program! source of many bugs!
						
						Flow is branched early, on condition of 'initialised':
						-if true, game loops, may ignore sertain system calls!
						
						-if false, just handle like any event, indefinite wait.
					*/
					if (initialised)
						//in game loop
					{
						EvtGetEvent(&event, 0);
						while (event.eType != nilEvent)
						{
								//a button press
							if (event.eType == keyDownEvent)
							{
								
								if (event.data.keyDown.modifiers & commandKeyMask)
									HardKeyHandleEvent(&event);
							} else if (event.eType == penUpEvent)
							{
									//pen -> grafiti soft button, keyDownEvent added on queue (needed for lanucher exit)
								SysHandleEvent(&event);
							}							
						
							EvtGetEvent(&event, 0);
							
						}
							/*
								QUICK FIX !!!
								NEED TO UPDATE EXIT PROCEDURE!!!
							*/
						if (gameStatusData.gameStatus != GAME_MANUAL_EXIT)
						{
								//run the game
							RunLoop();
								//delay
							SysTaskDelay(tenthOfaSecTick * gameStatusData.gameSpeed);
						} else {
							StopGame();
							LauncherExit();
						}
					} else {
					
							//check if return value is new game
						if (0)
						{

							
						} else {
						EvtGetEvent(&event, evtWaitForever);
						if (! FalseHardkeyEvent(&event))
						 if (! SysHandleEvent(&event))
							if (! MenuHandleEvent(0, &event, &MenuError))
								if (! AppHandleEvent(&event))
								FrmDispatchEvent(&event);
						}
					}
			 } while ((event.eType != appStopEvent));
	
         break;

	   default:
         break;
     }
	
   return(err);
 }
//////////////////////////////////////////////////////////////////
//user functions
//////////////////////////////////////////////////////////////////

	/*
		Function for handling hard key events
	*/
const char * vStr(moveVectType myVect)
{
	static char myStr[64], *tmpStr;
	if (myVect > 3)
	{
		StrCopy(myStr, "unknown!");
	} else {
		StrCopy(myStr, \
			(myVect == LEFT) ? "left" : ((myVect == UP) ? "up" : ((myVect == RIGHT) ? "right" : "down")) \
			);

	}
	return myStr;
}
	
static void CalcMovePosition(struct movePosition * myMovPos, moveVectType currMove)
{
	if (!initialised)
		return;
		/*	ignore the same position*/
	if (myMovPos->posNext == currMove)
		return;
		/*  oposite ways */
	if ((myMovPos->posNext + currMove) == HORIZONTAL_MOVE_SUM || \
		(myMovPos->posNext + currMove) == VERTICAL_MOVE_SUM)
		/*	igronring too, most reasonable action! */
		return;
	myMovPos->posNext = currMove;

}
			
static Boolean HardKeyHandleEvent(EventPtr hardKeyEvent)
{
		/*
			button:
				chr, keycode, modifiers
				
			applauncher button:
				NOTE: SysHandleEvent() must run after penUpEvent, enqueueing keyDownEvent, gives
				264, 0, 8
			up, down hard buttons
				11, 0, 8; 12, ...
			datebook
				516, 0, 8
			phonebook, todo, notepad (scribble), {power, a queue set}
				517, ...; 518; 519; {520... 286...powers off!}
			
			
		*/
	Boolean handled = false;


		//ignore if key is repeated
	if (keychr == hardKeyEvent->data.keyDown.chr)
		return false;
	keychr = hardKeyEvent->data.keyDown.chr;
			/*	30/05/2005:
				handling movements is simplified using CalcMovePosition function
			*/
	switch(keychr)
	{
	case vchrHard1:
		CalcMovePosition(&snakeMovPos, LEFT);
		handled = true;
		break;
	case vchrHard2:				//Address Book
		CalcMovePosition(&snakeMovPos, RIGHT);
		handled = true;
		break;
	case vchrHard3:				//ToDo
		//CalcMovePosition(&snakeMovPos, UP);
		handled = true;
		break;
	case vchrHard4:				//'Scribble' Notepad
		//CalcMovePosition(&snakeMovPos, DOWN);
		handled = true;
		break;
	case vchrPageUp:
		CalcMovePosition(&snakeMovPos, UP);
		handled = true;
		break;
	case vchrPageDown: //12
		CalcMovePosition(&snakeMovPos, DOWN);
		handled = true;
		break;
		/*
	case vchrHard5:
		DispText2("h5 button");
		handled = true;
		break;
	case vchrHard6:
		DispText2("h6 button");
		handled = true;
		break;
	case vchrRockerUp:
		DispText2("r up button");
		handled = true;
		break;
	case vchrRockerDown:
		DispText2("r down button");
		handled = true;
		break;
		//*/
	case 264:		//App Launcher button, exit!
		gameStatusData.gameStatus = GAME_MANUAL_EXIT;	//call for exit!
		handled = true;
		break;
	default:						//unknown!, unhandled
		return false;
	}
	return handled;
}


void LauncherExit(void)
{
			EventType menuLanuchEvent;
			
					MemSet(&menuLanuchEvent, sizeof(menuLanuchEvent), 0);
					menuLanuchEvent.eType = keyDownEvent;
					menuLanuchEvent.data.keyDown.chr = launchChr;
					menuLanuchEvent.data.keyDown.modifiers = commandKeyMask;
					EvtAddEventToQueue (&menuLanuchEvent);
}
	/*
		Main Loop of the game

		::violatile requirements, may remove later!
	*/
void RunLoop(void)
{
	//FormType *frmP;
	char statusStr[64];
	char CoordsStr[64];
			/*
				-delay
				-move cell
				-check status: if the cell has crashed
			*/
	if (gameStatusData.cellCount >= gameStatusData.levelMaxCells)
	{
		StopGame();
				/*
					-loose event handling:
					form quits when hard button exits
					
					14 june 2005
					One solution would be to:
					-replace FrmCustomAlert() with FrmDoDialog()
				*/
		//12 dec 2005
		if (gameStatusData.levelCount >= MAX_LEVELS)
		{
			//user has finished the game
			FrmAlert(EndOfGameAlert);
			LauncherExit();
			return;
		} else {
		
			StrPrintF(statusStr, "entering level %d... ", gameStatusData.levelCount+1);
			FrmCustomAlert(NextLevelAlert, "Well done! ", statusStr, "Starting new game");
			gameStatusData.levelCount++;
			
			if (gameStatusData.gameSpeed < 5)
			{
				gameStatusData.gameSpeed = 10;	//should be defined but hey, lets keep it simple
			} else {
				gameStatusData.gameSpeed--;
			}
			
			if (gameStatusData.levelMaxCells >= MAX_CELLS)
			{
				gameStatusData.levelMaxCells = MAX_LEVEL_CELLS;
			} else {
				gameStatusData.levelMaxCells++;
			}
			
			Initialise();
		}		
	}

	MoveCell();
	if (!initialised)
	{
		
		switch (gameStatusData.gameStatus)
		{
			case StopGameAppMenu:
					/*
						how should i handle this?
						22/03/2005
						lets attempt just to test gameStatusData.gameStatus in event queue
						and exit if null
						...
						forgot to do cleanup
					*/
					LauncherExit();
					 
				break;
			case StopGameNewGame:
					//delay for a 1/2 seconds
				SysTaskDelay(SysTicksPerSecond()/2);
					//erase form; reinitialise and reset level count
				FrmEraseForm(FrmGetActiveForm());
					//reset gameStatusData
				gameStatusData.levelCount = 0;
				gameStatusData.cellCount = 0;
				gameStatusData.levelMaxCells = MAX_LEVEL_CELLS;
				gameStatusData.gameSpeed = DEFAULT_SPEED;
				Initialise();
				break;
			case StopGameScores:
					/*	26/May/2005
						Display scores
					*/
					FrmPopupForm(ScoresForm);
				break;
			default:
				break;
		}
	} else {
		StrPrintF(CoordsStr, "Cur pos x: %d,\ty: %d\t", curPosX, curPosY);
		DispText(CoordsStr);
	}
}

	/*
		progress the snake
		::called by the timer 
		::x, y obtained from key buffer, hence no parameters
	*/
void MoveCell(void)
{
	//MemHandle moveCell;
	cellEntryType *moveCellPtr = NULL;
	crashType crashStatus;




	switch(snakeMovPos.posNext)	//assuming got input from keyboard
	{
	case LEFT:
		curPosX--;
		break;
	case UP:
		curPosY--;		//Y GROWS DOWNWARDS in this implementation!!!
		break;
	case RIGHT:
		curPosX++;
		break;
	case DOWN:
		curPosY++;
		break;
	default:
		break;
			/*	Alternative - thighter, but simpler?: 
			if ((snakeMovPos.posNext % 2) == 0) curPosX += (snakeMovPos.posNext < 2) ? -1 : 1; else curPosY += (snakeMovPos.posNext < 2) ? -1 : 1;
			*/
	}
			/*
			Modulus enables circular movement
			*/
	curPosX = (curPosX+CELL_COUNT_X)%CELL_COUNT_X;
	curPosY = (curPosY+CELL_COUNT_Y)%CELL_COUNT_Y;
	/*
		moving along the list

		::not very Efficient!!!
		::better to use LastHandle to draw, instead of RootHandle, hence using lock/unlock only once!
	*/
	
	crashStatus = CellCrash(curPosX, curPosY);
	if (crashStatus != SAFE)
	{
		CalcScores(crashStatus);
				/*	storing scores
				*/
		if (crashStatus != GROW)
		{
			if ( gameStatusData.currScores )
			{
				OpenDatabase();
				DbStoreCurrScore(&gameStatusData.currScores);
				CloseDatabase();
				gameStatusData.currScores = 0;
			}
		}
		
	}
	switch (crashStatus)
	{
	case GROW:
		AddNewCell(curPosX, curPosY,  _INV_VECTOR(snakeMovPos.posNext) );
		GenNewGrowCell();
		DrawRootCell();
		break;
	case CRUSHED:
		DispText("crashed!");
		StopGame();
			/*NOTE:  A default button press is simulated if the user switches to a different application while a modal dialog is active.
			*/
		gameStatusData.gameStatus = FrmCustomAlert(StopGameAlert, "Opps! ", "The snake has crashed into itself! ", NULL);
		return;
	case WALL_CRASHED:
		StopGame();
		gameStatusData.gameStatus = FrmCustomAlert(StopGameAlert, "End of Game ", "Crashed into the wall ", NULL);
		//gameStatusData.gameStatus = StopGameDialog(StopGameAlert);
		return;
	case OUTSIDE:
		//DispText("End of Game  ");
		//StopGame();
		//gameStatusData.gameStatus = FrmCustomAlert(StopGameAlert, "1", "2", "3");
		//return;
	case SAFE:
	default:
	EraseLastCell();

	LastHandle = RootHandle;
	moveCellPtr = (cellEntryType *) MemHandleLock(LastHandle);
			//needed?
		if (moveCellPtr == NULL)	return;
		RootHandle = moveCellPtr->Next;
		moveCellPtr->facePos =  _INV_VECTOR((char) snakeMovPos.posNext);
	MemHandleUnlock(LastHandle);
	moveCellPtr = (cellEntryType *) MemHandleLock(RootHandle);
			//the same, needed?
		if (moveCellPtr == NULL)	return;
		moveCellPtr->cellData.x = curPosX;
		moveCellPtr->cellData.y = curPosY;

	MemHandleUnlock(RootHandle);

	DrawRootCell();
	
	
	/*
		advance position
	*/

	}
	snakeMovPos.posPrev = snakeMovPos.posNext;
}

runStatusType StopGameDialog(UInt16 StopFormID)
{
		runStatusType whichButton;
		FormType *frmP;
		
		frmP = FrmInitForm(StopFormID);
		whichButton = FrmDoDialog(frmP);
		FrmDeleteForm(frmP);
		
		return whichButton;

}

	/*
		Setup screen area and data types
		::called in the beginning, once for each game
	*/
void Initialise(void)
{
	MemHandle tmpCell;
	cellEntryType *cellEntryPtr;
	Int16 startx, starty;
	/*
	Boolean setBool = true;
	 UInt32 Width, Height, Depth;
	 FormType *frmP = FrmGetActiveForm();
	// char HeightStr[64];
	

		//set the Correct Colour Depth
	WinScreenMode(winScreenModeSet, NULL, NULL, &setDepth, &setBool);
	    WinScreenMode(winScreenModeGetSupportedDepths, NULL, NULL, &Depth, NULL);
	if (!(Depth & 8000))
	{
	    FrmNewLabel(&frmP, 1003, "Cannot set 65k colour mode!", 10, 45, 0);
		return;
	}
	*/
		/* 
			graphics resources
		*/
	SetColour();
	
	if( LoadBitmaps() )
	{
		// oops!
	}
	

		//create two cells	
	RootHandle = MemHandleNew(sizeof(cellEntryType));
	LastHandle = MemHandleNew(sizeof(cellEntryType));
			
			//default level
	if (!gameStatusData.levelCount) gameStatusData.levelCount = START_LEVEL;
	
	startx = DEF_START_X_POS;
	starty = DEF_START_Y_POS;
	
	while(!_IS_FREE(startx, starty, gameStatusData.levelCount) || !_IS_FREE(startx-1, starty, gameStatusData.levelCount))
	{
		//generate new cell that is not in the wall
		startx = SysRandom(TimGetTicks ());
		starty = SysRandom(DEF_START_Y_POS);
		startx %= CELL_COUNT_X;
		starty %= CELL_COUNT_Y;
	}
	
	cellEntryPtr = (cellEntryType *) MemHandleLock(RootHandle);
		if(cellEntryPtr == NULL)
				return;
		cellEntryPtr->Next = LastHandle;
		cellEntryPtr->cellData.x = startx;
		cellEntryPtr->cellData.y = starty;
	MemHandleUnlock(RootHandle);
	cellEntryPtr = (cellEntryType *) MemHandleLock(LastHandle);
		if(cellEntryPtr == NULL)
				return;
		cellEntryPtr->Next = RootHandle;
		cellEntryPtr->cellData.x = curPosX = startx-1;
		cellEntryPtr->cellData.y = curPosY = starty;
	MemHandleUnlock(LastHandle);
		/*
			initialised variables
				curPosX
				curPosY
		*/
		//Default number of cells;
	gameStatusData.cellCount = 2;
		//Status of game is running
	gameStatusData.gameStatus = 2;
		//Default move vector position
	snakeMovPos.posNext = snakeMovPos.posPrev =		LEFT;

	keychr = (WChar) NULL;
	//draw BackGround
	DrawBackGround();
	
		
	tmpCell = RootHandle;
	RootHandle = LastHandle;
		//Draw Initial Cells
	DrawRootCell();
	RootHandle = tmpCell;
	DrawRootCell();
		//Draw Grow Cell
	GenNewGrowCell();

	initialised = true;
		/*	
			User Feedback about starting the game
		*/
	FrmAlert(GameStartAlert);
}

	/*
		Cleanup before exit
	*/
void StopGame(void)
{
	RectangleType formRect;
		

		
		//Erase the form
	//does not work!!! FrmEraseForm(FrmGetActiveForm());
	formRect.topLeft.x = 0;
	formRect.topLeft.y = 0;
	formRect.extent.x = WIN_WIDTH;
	formRect.extent.y = WIN_HEIGHT;
	
	WinEraseRectangle(&formRect, 0);
	
	if (initialised)
	{
			//remove the list
		initialised = false;	
			//Destroy Bitmap Handles
		UnloadBitmaps();
		FlushList();
	}
	
	if (!gameStatusData.gameStatus)
	{
		gameStatusData.cellCount = 0;
		gameStatusData.levelCount = 0;
	}
}

	/*
		Crashed,
		::either into:
		::a)itself, game finishes
		::b)into the growing cell, that grows the snake
	*/
crashType CellCrash(Int16 xm, Int16 ym)
{
	//cellEntryType *rootCellPtr;

	if (!InWindowLimits(xm, ym))
			//outside of window, alas!
	{
		DispText("out of windows bounds");
		return OUTSIDE;
	}

	if (FindCellInList(xm, ym))
	{
		DispText("Snake has Crashed!");
		return CRUSHED;
	}
	
	if ( _IS_WALL(levelsData[gameStatusData.levelCount-1][ym][xm]))
	{
		return WALL_CRASHED;
	}
		//check whether it crashed in itself!!

	//rootCellPtr = (cellEntryType *) MemHandleLock(RootHandle);
	//if (rootCellPtr->cellData.x == curPosX && rootCellPtr->cellData.y == curPosY)
	if (xm == growTokenX && ym == growTokenY)
		return GROW;
	return SAFE;
}

void GenNewGrowCell(void)
{
		/*
			Random position for next Food Block for snake
			
			22/03/2005
			New FoodBlock must not be part of the snake,
			::enter do loop,
			::1)generate new random values for x and y coordinates
			::in while:
			::2)check in FindCellInList(x, y), if not found...
			::3)try see if cells are wall?
			::4)if neither are true, grow token is save, exit loop, else try again
		*/

	do
	{
		growTokenX = SysRandom (TimGetTicks () / (curPosY % (CELL_COUNT_X - 1) + 1));
		growTokenY = SysRandom (TimGetTicks () % (curPosX / 2 + 1) + ( (UInt32)RootHandle / (curPosY % (CELL_COUNT_Y - 1) + 1) ));
		growTokenX %= CELL_COUNT_X;
		growTokenY %= CELL_COUNT_Y;		
	}
	while (FindCellInList(growTokenX, growTokenY) || _IS_WALL(levelsData[gameStatusData.levelCount-1][growTokenY][growTokenX]) );


	WinPaintBitmap(cellBmpPtrArray[CELL_FOOD], growTokenX * CELL_DIM_X, growTokenY * CELL_DIM_Y);

}

	/*
			Add a new Cell into linked list
	*/
UInt16 AddNewCell(Int16 new_x, Int16 new_y, moveVectType myPos)
{
	MemHandle nextCell;
	cellEntryType *curCellPtr = NULL;

	gameStatusData.cellCount++;

		/*
			Inserting new Cell
			::Operations here:
			::1)move RootHandle to LastHandle;
			::2)create new root handle
			::3)open previous root handle, new LastHandle
			::	a)save Next handle
			::	b)set Next to new RootHandle
			::4)Update data (x, y), Next handle in the new RootHandle
		*/
	LastHandle = RootHandle;									//1)
	RootHandle = MemHandleNew(sizeof(cellEntryType));			//2)
	curCellPtr = (cellEntryType *) MemHandleLock(LastHandle);	//3)
		if (curCellPtr == NULL)
				//null pointer, error
			return 2;
				//the SALT of inserting new link
		nextCell = curCellPtr->Next;							//a)
		curCellPtr->Next = RootHandle;							//b)
		curCellPtr->facePos = myPos;		//face for future tail position
	MemHandleUnlock(LastHandle);
	
	curCellPtr = NULL;
	curCellPtr = (cellEntryType *) MemHandleLock(RootHandle);	//4)
		if (curCellPtr == NULL)
				//null pointer, error
			return 2;
		curCellPtr->cellData.x = new_x;
		curCellPtr->cellData.y = new_y;
		curCellPtr->Next = nextCell;
	MemHandleUnlock(RootHandle);

	return 0;
}

		//should stay indemendent of the implementation of drawEntry
void FlushList(void)
 {
	MemHandle Handle, NextHandle;
	cellEntryType *curCellPtr = NULL;
	
	if ((!RootHandle) || (!LastHandle))
		return;

	Handle = RootHandle;
		//important to empty Root Handle!? your application may crash otherwise!!!
	RootHandle = NULL;
		//breack the curcular reference
		//::list terminates with NULL now!
	curCellPtr = (cellEntryType *) MemHandleLock(LastHandle);
		if (curCellPtr == NULL)
			return;
		curCellPtr->Next = NULL;
	MemHandleUnlock(LastHandle);
   
		//emty lastHandle
	LastHandle = NULL;
   
   
	while (Handle)
		//release the date
	{
		curCellPtr = (cellEntryType *) MemHandleLock(Handle);
		if (curCellPtr != NULL)
			NextHandle = curCellPtr->Next;
				//unlock
		MemHandleUnlock(Handle);
		MemHandleFree(Handle);
		Handle = NextHandle;
	}
}

	/*
		Whether position is inside window boundaries
		::In limits, if value > 0
		::assume returning values 0 or 1 are sufficient!
		::~function may return positive (from inside the window+1*) or negative (distance from outside)
		::		distance from the boundary
		::*touches window from inside == 1
	*/
Int16 InWindowLimits(Int16 x, Int16 y)
{
	char CoordsStr[63];
	StrPrintF(CoordsStr, "new value %dx%d%s                  ", x, y, (y < 15)? ", out of limits!": "");
	//WinDrawChars(CoordsStr, StrLen(CoordsStr), 30, 130);
	//i know, too complicated, could be in 1 line!
	/* if (	x >= WIN_X_OFFSET	&& x < WIN_X_OFFSET+WIN_WIDTH \
		&&	y >= WIN_Y_OFFSET	&& y < WIN_Y_OFFSET+WIN_HEIGHT)
		*/
	if (	x >= 0	&& x < CELL_COUNT_X \
		&&	y >= 0	&& y < CELL_COUNT_Y)
		return 1;
	 else 
		return 0;
}

UInt16 FindCellInList(Int16 xm, Int16 ym)
{
		/*
			13/04/2005
			-The bottle-neck in the algorithm! create a more efficient algorithm
			for speedup, possibly in the future
		*/
		
	MemHandle listHandle, nextHandle;
	cellEntryType *listPtr;
	UInt16 cellFound = 0;

	listHandle = RootHandle;

	do {
		listPtr = (cellEntryType *) MemHandleLock (listHandle);
		if (listPtr == NULL)
			return 0;
		if (listPtr->cellData.x == xm && listPtr->cellData.y == ym)
			//wow, a crash!
		{
			MemHandleUnlock(listHandle);
			return 1;
		}
		nextHandle = listPtr->Next;	
		MemHandleUnlock(listHandle);
		listHandle = nextHandle;
	} while (listHandle != RootHandle);		//assuming circular consistency
	
	return cellFound;
}

void CalcScores(crashType crashRate)
{
		/*
			14 june 2005
			Score counting algorithm
		*/
	#define M_CUT_RATE 160
	
	int levelRating =	(gameStatusData.levelCount + 10) * 10;
	int lengthRating =	gameStatusData.cellCount * 80;
	int metricRating =	SQR(curPosX - CELL_COUNT/2)  + SQR(curPosY - CELL_COUNT/2);
	
	metricRating = (metricRating > 	M_CUT_RATE) ? metricRating - M_CUT_RATE : 0;
	
	switch (crashRate)
	{
		case GROW:
			gameStatusData.currScores += lengthRating * metricRating / 5;
			break;
		case CRUSHED:
		case OUTSIDE:
			gameStatusData.currScores += lengthRating * metricRating + (lengthRating + levelRating * 2) * 2;
			break;
		default:	
			break;
	}
	//gameStatusData.currScores = borderMetric * 100 + (levelCount + 10) * 10 * cellCount ;
}

/*	12/05/2005
	This algoritm is not ready yet!
	
	(a, b) -> *2 - 3 -> {* (a + b)}  + {* (1 - MODULUS(a + b) / 4)} -> (a - b) -> {-8, -2, 8, 2}
	
	May be implemented as One! expression, too complicated at the moment!
	-------------------------------------
	13 Sep 2005
	There is a MUCH simpler solution!!!
	adding vectors in below matrixes, produces an absolute values of
	{3,4,1,8} that correspont to four pictures on bends
	
	the macro processes the curr, where snakes Turns inTo 
*/
BitmapType * BentBitmapAlgorithm(moveVectType curr, moveVectType prev)
{
	#ifdef FOUR_BENDS
		switch(ABS(prev + _TURN2(curr) ))
		{
			case 1:
				return cellBmpPtrArray[SNK_BENT_NE];
			case 3:
				return cellBmpPtrArray[SNK_BENT_NW];
			case 4:
				return cellBmpPtrArray[SNK_BENT_SW];
			case 8:
			default:
				return cellBmpPtrArray[SNK_BENT_SE];
		}
	#else	//2 bends
		/* if modulus remainder is 0, than TopLeft
		*/
		switch (ABS(curr + prev))
		{
			case 4:
			case 1:
				return cellBmpPtrArray[SNK_HEAD_N];
			case 3:
			case 8:
			default:
				return cellBmpPtrArray[SNK_HEAD_S];
		}
	#endif
}


	/*
		Determine what bitmap to draw after the head
		-if the snake turned, draw specific bitmap (must be >2 in length)
		
		-using pointer for extensibility / abstraction
			^ 1
		2	| 	  0
		<-		->
			| 3
			~
		13 Sep 2005
		CHANGE!!!, first vector set is
			^ 1
		4	| 	  0
		<-		->          3        0
			| 8
			~
		(could be {3,6} instead of {4,8}, though harder to calculate next position)
		second is (how similar to matrix inverse!?!):
			^ -8
		0	| 	  -4
		<-		->
			| -1
			~
		
	*/
	
BitmapType * GetHeadBitmapPtr(struct movePosition * myMovPos)
{
			/* A complicated, though comprehensible solution!
			easier, mapping, {0, 1, 2, 3} (x-SNK_HEAD_N) -> {0, 1, 4, 8}:
			
				(x < 2) ? x : ((x-1) << 2)
			*/
	switch(myMovPos->posNext)
	{
		case RIGHT:
			return cellBmpPtrArray[SNK_HEAD_E];
		case UP:
			return cellBmpPtrArray[SNK_HEAD_N];
		case LEFT:
			return cellBmpPtrArray[SNK_HEAD_W];
		case DOWN:
		default:
			return cellBmpPtrArray[SNK_HEAD_S];
	}
}

BitmapType * GetBodyBitmapPtr(struct movePosition * myMovPos)
{
	cellBitmapType snkBentBitmap = SNK_DFT;
	
	if (myMovPos->posNext == myMovPos->posPrev || gameStatusData.cellCount <= 2)
	{
		switch(myMovPos->posNext & 9) // horizontal - b0xx0 (& b1001 == 0)
		{
			case 0:
				return cellBmpPtrArray[SNK_BODY_H];
			default:
				return cellBmpPtrArray[SNK_BODY_V];
		}
	} else {
				return BentBitmapAlgorithm(myMovPos->posNext, myMovPos->posPrev);
	}
	return cellBmpPtrArray[SNK_HEAD_W];
}

BitmapType * GetTailBitmapPtr(cellEntryType * myCell)
{
	
	switch(myCell->facePos)
	{
		case UP:
			return cellBmpPtrArray[SNK_TAIL_N];
		case RIGHT:
			return cellBmpPtrArray[SNK_TAIL_E];
		case DOWN:
			return cellBmpPtrArray[SNK_TAIL_S];
		case LEFT:
		default:
			return cellBmpPtrArray[SNK_TAIL_W];
	}
}

	/*
		Drawing of the snake  (head)

	*/
void DrawRootCell(void)
{
	cellEntryType * cellEntryPtr;
	RectangleType clearRect;

	cellEntryPtr = (cellEntryType *) MemHandleLock(RootHandle);
		if(cellEntryPtr == NULL) 
			return;
		/*	clear draw area
		*/
	clearRect.topLeft.x = cellEntryPtr->cellData.x * CELL_DIM_X;
	clearRect.topLeft.y = cellEntryPtr->cellData.y * CELL_DIM_Y;
	clearRect.extent.x = CELL_DIM_X;
	clearRect.extent.y = CELL_DIM_Y;
	WinEraseRectangle(&clearRect, 0);
		WinPaintBitmap(GetHeadBitmapPtr(&snakeMovPos),	cellEntryPtr->cellData.x * CELL_DIM_X, \
														cellEntryPtr->cellData.y * CELL_DIM_Y);
	MemHandleUnlock(RootHandle);
	
		/*
			draw the next cell after head
			LastHandle is the next cell after RootHandle
		*/
	cellEntryPtr = (cellEntryType *) MemHandleLock(LastHandle);
	
	clearRect.topLeft.x = cellEntryPtr->cellData.x * CELL_DIM_X;
	clearRect.topLeft.y = cellEntryPtr->cellData.y * CELL_DIM_Y;
	WinEraseRectangle(&clearRect, 0);
		WinPaintBitmap(GetBodyBitmapPtr(&snakeMovPos),	cellEntryPtr->cellData.x * CELL_DIM_X, \
														cellEntryPtr->cellData.y * CELL_DIM_Y);
	MemHandleUnlock(LastHandle);
}

void EraseLastCell(void)
{
	moveVectType myPos; //debug, remove if not needed!
	/*
		08/01/2005:
		complete rewrite:
			-previous algorithm: 
				simply erase LastHandle, which comes after<!> roothandle
			-new, correct version
				copy next handle from root, which is going to be erased, and erase it!
				also, it needs to be called by MoveCell() instead of RunLoop()!
	*/
	
	//BitmapType *newBackgroundPtr;
	
	MemHandle afterRootCell, tail;
	cellEntryType *rootCellPtr = (cellEntryType *) MemHandleLock(RootHandle);
		//test for NULL if you like here
	afterRootCell = rootCellPtr->Next;
	MemHandleUnlock(RootHandle);

	rootCellPtr = (cellEntryType *) MemHandleLock(afterRootCell);
			/* Draw the thing! */
		DrawCell(rootCellPtr, cellBmpPtrArray[CELL_BG]);
			/* Catch the tail */
		tail = rootCellPtr->Next;
	MemHandleUnlock(afterRootCell);
	
	rootCellPtr = (cellEntryType *) MemHandleLock(tail);
		myPos = rootCellPtr->facePos;
		//StrPrintF(debugStr, "tail position: %s", (myPos == UP)? "up" :((myPos == DOWN) ? "down" :((myPos == LEFT)? "left" : "right")));
		//DispText2(debugStr);
		//rootCellPtr->facePos = UP;
		DrawCell(rootCellPtr, GetTailBitmapPtr(rootCellPtr));
	MemHandleUnlock(tail);
}

void DrawCell(cellEntryType * tmpCellPtr, BitmapType * bitmapPtr)
{
	RectangleType clearRect;
	
	clearRect.topLeft.x = tmpCellPtr->cellData.x * CELL_DIM_X;
	clearRect.topLeft.y = tmpCellPtr->cellData.y * CELL_DIM_Y;
	clearRect.extent.x = CELL_DIM_X;
	clearRect.extent.y = CELL_DIM_Y;
	
	WinEraseRectangle(&clearRect, 0);
	WinPaintBitmap(bitmapPtr, tmpCellPtr->cellData.x * CELL_DIM_X, tmpCellPtr->cellData.y * CELL_DIM_Y);
}

void DrawBackGround(void)
{

	Coord i, j;

	for (j = 0; j < CELL_COUNT_Y; j++)
		for (i = 0; i < CELL_COUNT_X; i++)
		{
				//fill the window with bitmap
				//i, j count cells, CELL_DIM_axis are cell dimensions
			WinPaintBitmap(cellBmpPtrArray[CELL_BG], i * CELL_DIM_X, j * CELL_DIM_Y);
				//draw level details, wall
			if( _IS_WALL(levelsData[gameStatusData.levelCount-1][j][i]) )
			{
				WinPaintBitmap(cellBmpPtrArray[CELL_WALL], i * CELL_DIM_X, j * CELL_DIM_Y);
			}
			
		}
}

	/*	
		Loading bitmaps
		
		functions works only once
	*/

Err	LoadBitmaps(void)
{
	static int arraysNullified = 0;
		//temporary bitmap data
	MemHandle tmpBmpHandle;	BitmapType *tmpBmpPtr;

	if (!bitmapsLoaded)
	{
		if (!arraysNullified)
		{
			int i;
			for (i = 0; i <= CELL_BITMAP_MAX; i++)
			{
				cellBmpPtrArray[i] = NULL;
			}
			
			arraysNullified = !arraysNullified;
		}
		/*
			???do i need to check for NULL's in resources acquisition and handle locks?
		*/

			/*
				get handles from resources for bitmaps
				
				not checking for nulls here...
				
				tmpHandle = DmGetResource (...);
				tmpBmpPtr = MemHandleLock (...);
				foreach(i=0..3)
				{
					cellBmpPtrArray[SNAKE_ITEM_N + i] = BmpCreate(...);
					PopulateBitmaps(cellBmpPtrArray[SNAKE_ITEM_N+i], tmpBmpPtr, IDENTITY+i);
				}
				MemHandleUnlock(tmpHandle);
				DmReleaseResource(...);
				
				------------
				deleting bitmaps
				------------
				foreach(i=0..all bitmaps)
				{
					if (cellBmpPtrArray[i])!= 0
			*/		
		tmpBmpHandle =		DmGetResource	(bitmapRsc, StaticRscBitmapFamily);
		tmpBmpPtr =		 	MemHandleLock	(tmpBmpHandle);
			LoadStaticBitmaps(tmpBmpPtr);
							MemHandleUnlock	(tmpBmpHandle);
							
							DmReleaseResource(tmpBmpHandle);
		tmpBmpHandle =		DmGetResource	(bitmapRsc, SnakeRscBitmapFamily);
		tmpBmpPtr =			MemHandleLock	(tmpBmpHandle);
			LoadSnakeBitmaps(tmpBmpPtr);
							MemHandleUnlock	(tmpBmpHandle);
							DmReleaseResource(tmpBmpHandle);

		/* let LEVELS LOAD HERE too!
		*/
		tmpBmpHandle =		DmGetResource	(bitmapRsc, LevelsBitmapFamily);
		tmpBmpPtr =			MemHandleLock	(tmpBmpHandle);
			LoadLevels(tmpBmpPtr);
							MemHandleUnlock	(tmpBmpHandle);
							DmReleaseResource(tmpBmpHandle);
		#ifndef FOUR_BENDS	//2 bends
		#else				//4 bends
		#endif
		bitmapsLoaded = !bitmapsLoaded;
	}
	return 0;
		
}

void UnloadBitmaps(void)
{
	/*
			if array of Handles is initianised -> unused entries are NULL,
			otherwise released
	*/
	static int i;
	
	if (bitmapsLoaded)
	{
	
			for (i = 0; i <= CELL_BITMAP_MAX; i++)
			{
				if (cellBmpPtrArray[i] != NULL) //or.. if (cellBmpHdlArray[i])
				{
						/*
							no more handle play...
							just delete all bitmaps created with Load*Bitmaps()
						*/
					BmpDelete		(cellBmpPtrArray[i]);
						/*
							and nullify arrays of pointers
						*/
					cellBmpPtrArray[i] = NULL;
				}
			}
		bitmapsLoaded = !bitmapsLoaded;
	}
}

void SetColour(void)
{
	static char initialised = 0;
	UInt32 depth;
	Boolean enable = true;
	
	if (initialised)
		return;
		/*	Get the depth integer for masking colours support */
	WinScreenMode(winScreenModeGetSupportedDepths, NULL, NULL, &depth, NULL);
	
	/*
		Colour masks:
		
		0x8000	16bit		(m505, mine!) draws only in 256 colours!?!
		0x80	8bit, 256c	dont know if works at all..., will support by default!
		0x8		4bit, 16c	
		0x2		2bit, 4c	problem supporting, needs low level coding!
		0x1		bw			nice and simple
	*/
	
	if (depth &= 0x8000)		//16bit, loose initialisation here...
	{
		gameStatusData.unFlags.deviceFlags.Colour = 0x1;
		WinScreenMode(winScreenModeSet, NULL, NULL, &depth, &enable);
	} else {
		gameStatusData.unFlags.deviceFlags.Colour = 0x0;
		WinScreenMode(winScreenModeSet, NULL, NULL, &depth, &enable);
	}
	
	initialised = !initialised;
}

//-------------------------------

void DispText(const char * txt)
{
	static UInt16 txtLen;
	txtLen = StrLen(txt);
	//WinDrawChars(txt, txtLen, 5, 15);
}

void DispText2(const char * txt)
{
	static UInt16 txtLen;
	txtLen = StrLen(txt);
	WinDrawChars(txt, txtLen, 5, 23);
}


